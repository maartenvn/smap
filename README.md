# SMAP - Stack Mapping Anchor Points

## Description

**SMAP** is a software package that analyzes 'Stack-based' Illumina short read data, such as Genotyping-By-Sequencing (GBS) or highly multiplex amplicon sequencing (HiPlex), and extracts haplotype calls.
**SMAP** complements existing algorithms by focussing on highly diverse outbreeding species where multiple polymorphisms typically occur within the length of a single short read. 
In addition, **SMAP** takes polymorphisms in read mapping positions into account as additional molecular marker information.
**SMAP** only needs a set of BAM files and a VCF file as input and creates haplotypes directly from sequencing reads (‘ShortHaps’) using read-backed haplotyping. SMAP consists of three components:
1. **SMAP delineate** builds a catalogue of Stacks within BAM files, whereby the start and end of stacks of reads define Stack Mapping Anchor Points (SMAPs). SMAP-delineate then merges Stacks within a BAM file to create StackClusters. These StackClusters across multiple BAM files are then merged to build a catalogue of MergedClusters.
2. **SMAP compare** identifies the number of common MergedClusters across two runs of SMAP-delineate. It is a useful tool to determine the number of common loci targeted in GBS of different populations or sample sets.
3. **SMAP haplotype-sites** generates haplotype calls (‘ShortHaps’) using read-backed haplotyping with polymorphisms at SMAPs (from SMAP-delineate) and SNPs (as VCF obtained from third-party algorithms) for the same set of BAM files. It creates a table with discrete haplotype dosage (individual genotypes) or relative haplotype frequencies (pool-Seq).


The scheme below displays a global overview of the functionalities of the SMAP-package. White ovals are external operations and grey ovals are components of SMAP. Square boxes show the output of each of the components. Arrows show how output from various components are required input for the next component in the workflow for both of the NGS library types (GBS and HiPlex), file formats are shown in uppercase italics.

![Quick overview of the SMAP workflow parameters](/docs/images/SMAP_global_overview_sites.png)

## Documentation

An extensive manual of the SMAP package can be found on [Read the Docs](https://ngs-smap.readthedocs.io/) including detailed explanations and illustrations.

## Citation

If you use SMAP, please cite "Ruttink, T. (2021) SMAP: a versatile approach to read-backed haplotyping in stacked NGS read data. [Online]. Available online at https://gitlab.com/truttink/smap/"

## Building and installing

SMAP is being developed and tested on Linux.
Additionally, some dependencies are only developed on Linux.

### Via Git

1. `git clone https://gitlab.com/truttink/smap.git`
2. `cd smap`
3. `git checkout master`
4. `python3 -m venv .venv`
5. `source .venv/bin/activate`
6. `pip install --upgrade pip`
7. `pip install .`

or 

`git clone https://gitlab.com/truttink/smap.git ; cd smap ; git checkout master ; python3 -m venv .venv ; source .venv/bin/activate ; pip install --upgrade pip ; pip install .`

### Using Docker
A docker container is available on dockerhub. 
To pull the docker image and run SMAP using Docker, use:
`docker run dschaumont/smap --help`
