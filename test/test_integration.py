import unittest
from smap.haplotype import Stacks
from smap.delineate import MergedClusters
from io import StringIO

class TestDelineateHaplotypeWindow(unittest.TestCase):
    def setUp(self):
        self.merged_clusters = MergedClusters({
            0: {'chr': 1, 'cluster_count': 3, 'cluster_depth_collapse': [100, 100, 100], 
                'end': 115, 'end_collapse': [115, 115, 115], 'sample_count': 2, 'start': 6, 
                'start_collapse': [6, 6, 6], 'strand': '+'},
            1: {'chr': 1, 'cluster_count': 4, 'cluster_depth_collapse': [75, 25, 80, 100], 
                'end': 354, 'end_collapse': [354, 344, 354, 354], 'sample_count': 3, 'start': 245, 
                'start_collapse': [245, 245, 245, 245], 'strand': '+'}})

    def test_read_delineate_output(self):
        result = {
            "1:6-115/+": {'positions': {114, 6}, 'scaffold': '1', 'smaps': ['6', '114'],
                          'start': 6, 'stop': 115, 'strand': '+', 'variants': {}},
            "1:245-354/+": {'positions': {353, 245, 343}, 'scaffold': '1', 'smaps': ['245', '343', '353'], 
                            'start': 245, 'stop': 354, 'strand': '+', 'variants': {}}
            }
        bed_buffer = StringIO()
        self.merged_clusters.write_to_bed(bed_buffer, 'Set1')
        bed_buffer.seek(0,0)
        stacks = Stacks(bed_buffer)
        self.assertDictEqual(stacks.stacks, result)