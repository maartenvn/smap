.. SMAP documentation master file, created by
   sphinx-quickstart on Wed Aug  5 13:28:17 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Haplotyping for different sample types (individual or Pool-Seq)
===============================================================

Individual and Pool-Seq samples are different in two important ways; 
	
	*	In individuals, low frequency haplotypes (<5%) are generally noise and need to be removed from the data. In Pool-Seq, it may be difficult to discriminate between noise and low frequency true alleles.
	*	In Pool-Seq, haplotype frequencies represent the genetic diversity of the population. For individuals, haplotype frequencies should be converted into discrete genotype calls, which can either be expressed as dominant (0/1) or dosage (0/1/2, diploid; O/1/2/3/4, tetraploid).
	
| Therefore, these two sample types are discussed separately. Tables with haplotype frequencies are always calculated, while specific options are available to perform discrete genotype calling in individual samples.

.. image:: ../../images/sites/haplotype_step_scheme_34.png

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   sites_individuals_HIW
   sites_pools_HIW

